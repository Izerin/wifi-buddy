import csv
import os
import csv

def outputToCSV(input_file, output_file, field_names):
    with open(input_file, 'r', encoding='utf-8') as file:
        log_data = file.read()

    chunks = log_data.strip().split('\n\n')
    rows = []

    for chunk in chunks:
        row = {}
        lines = chunk.strip().split('\n')
        for line in lines:
            field, data = line.split(': ', 1)
            if field in field_names:
                row[field] = data
        rows.append(row)

    with open(output_file, 'w', newline='', encoding='utf-8') as file:
        writer = csv.DictWriter(file, fieldnames=field_names)
        writer.writeheader()
        writer.writerows(rows)