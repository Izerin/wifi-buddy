import os
import time
import pywifi
from output_to_csv import outputToCSV
from pywifi import const
import subprocess
import shutil
import zipfile
import ctypes
import socket
import re
from tqdm import tqdm
import msvcrt
import sys
from itertools import cycle
import netifaces as ni
import iperf3
import threading
import signal

print("--------------------------------------")
print("|     SMARTAIRA WI-FI BUDDY a1.0     |")
print("--------------------------------------\n")

# Set the log file path on the desktop
log_file = os.path.join(os.path.expanduser("~"), "rssi.txt")
csv_file = os.path.join(os.path.expanduser("~"), "rssi.csv")
ping_file = os.path.join(os.path.expanduser("~"), "ping.txt")
iperf_file = os.path.join(os.path.expanduser("~"), "iperf3.txt")
wlan_networks_file = os.path.join(os.path.expanduser("~"), "wlan-networks.txt")
wlan_networks_csv_file = os.path.join(os.path.expanduser("~"), "wlan-networks.csv")
wlan_report_archive_file = os.path.join(os.path.expanduser("~"), "wlan-report.zip")
raw_wlan_networks_file = os.path.join(os.path.expanduser("~"), "cmd-netsh_show_wlan_networks.txt")
netsh_interface_file = os.path.join(os.path.expanduser("~"), "client-interface.txt")
zip_file = os.path.join(os.path.expanduser("~"), "Desktop", "Smartaira WiFi Buddy Report.bin")

# Define the noise floor value for the specific wireless standard
NOISE_FLOOR = -95  # Noise floor for 802.11b/g networks

def create_zip_archive():
    with zipfile.ZipFile(zip_file, "w", zipfile.ZIP_DEFLATED) as archive:
        archive.write(wlan_networks_csv_file, os.path.basename(wlan_networks_csv_file))
        archive.write(csv_file, os.path.basename(csv_file))
        archive.write(iperf_file, os.path.basename(iperf_file))
        archive.write(ping_file, os.path.basename(ping_file))
        archive.write(wlan_report_archive_file, os.path.basename(wlan_report_archive_file))
        archive.write(raw_wlan_networks_file, os.path.basename(raw_wlan_networks_file))
        archive.write(netsh_interface_file, os.path.basename(netsh_interface_file))

    # Delete the original files
    os.remove(log_file)
    os.remove(ping_file)
    os.remove(csv_file)
    os.remove(iperf_file)
    os.remove(wlan_networks_file)
    os.remove(wlan_networks_csv_file)
    os.remove(wlan_report_archive_file)
    os.remove(raw_wlan_networks_file)
    os.remove(netsh_interface_file)

def is_running_with_elevated_permissions():
    try:
        # Check if the script is running with elevated permissions (admin)
        return ctypes.windll.shell32.IsUserAnAdmin() != 0
    except AttributeError:
        # If ctypes or shell32 are not available, fallback to checking the process elevation
        return os.getuid() == 0
def scan_wifi_networks():
    wifi = pywifi.PyWiFi()  # Initialize the PyWiFi object
    iface = wifi.interfaces()[0]  # Get the first wireless interface

    iface.scan()  # Scan for available networks
    time.sleep(2)  # Wait for the scan to complete

    networks = iface.scan_results()  # Get all available networks
    return networks

def collect_wifi_info(network):
    ssid = network.ssid.encode("utf-8", errors="ignore")  # Get the SSID of the network
    rssi = network.signal  # Get the RSSI value (in dBm)
    snr = rssi - NOISE_FLOOR  # Calculate SNR estimation
    return ssid, rssi, snr

def get_netsh_info():
    result = subprocess.run(["netsh", "wlan", "show", "interface"], capture_output=True, text=True)
    netsh_output = result.stdout

    # Write netsh output to file
    with open(netsh_interface_file, "w", encoding="utf-8") as file:
        file.write(netsh_output)

    return netsh_output

def log_wlan_networks_info():
    result = subprocess.run(["netsh", "wlan", "show", "networks", "mode=Bssid"], capture_output=True, text=True)
    netsh_output = result.stdout

    # Write netsh output to file
    with open(raw_wlan_networks_file, "w", encoding="utf-8") as file:
        file.write(netsh_output)

    # Split the netsh output into individual network sections
    network_sections = netsh_output.split("\n\n")

    for section in network_sections:
        lines = section.splitlines()
        network_bssid = None
        network_ssid = None
        network_band = None
        network_channel = None
        network_signal = None

        for line in lines:
            if line.strip().startswith("SSID"):
                network_ssid = line.split(":", 1)[1].strip()
            elif line.strip().startswith("Band"):
                network_band = line.split(":", 1)[1].strip()
            elif line.strip().startswith("Band"):
                network_band = line.split(":", 1)[1].strip()
            elif line.strip().startswith("Channel"):
                network_channel = line.split(":", 1)[1].strip().split()[0]
            elif line.strip().startswith("Signal"):
                network_signal = line.split(":", 1)[1].strip()

        append_wlan_networks_info(network_ssid, network_band, network_channel, network_signal)


def log_pywifi_info(ssid, rssi, snr):
    timestamp = time.strftime("%Y-%m-%d %H:%M:%S")

    with open(log_file, "a", encoding="utf-8") as file:
        file.write(f"Timestamp: {timestamp}\n")
        file.write(f"SSID: {ssid.decode('utf-8')}\n")
        file.write(f"RSSI: {rssi} dBm\n")
        file.write(f"SNR: {snr} dB\n")
        file.write("\n")

def append_wlan_networks_info(ssid, band, channel, signal):
    timestamp = time.strftime("%Y-%m-%d %H:%M:%S")

    with open(wlan_networks_file, "a", encoding="utf-8") as file:
        file.write(f"Timestamp: {timestamp}\n")
        file.write(f"SSID: {ssid}\n")
        file.write(f"Band: {band}\n")
        file.write(f"Channel: {channel}\n")
        file.write(f"Signal: {signal}\n")
        file.write("\n")

def run_subprocess_with_output(command):
    # Run the subprocess command and capture the output
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)

    # Read the output and error streams line by line
    while True:
        line = process.stdout.readline().decode("utf-8")
        if not line and process.poll() is not None:
            break
        print(line.rstrip())

    # Get the error output
    error = process.stderr.read().decode("utf-8")

    # Display the error, if any
    if error:
        print("Error:")
        print(error)
def create_wlan_report_zip():
    # Run netsh command to generate the WLAN report
    run_subprocess_with_output(["netsh", "wlan", "show", "wlanreport"])

    # Get the path to the latest WLAN report files
    wlan_report_dir = os.path.join("C:\\ProgramData\\Microsoft\\Windows\\WlanReport")
    latest_report_files = get_latest_report_files(wlan_report_dir)

    # Set the path for the final zip file
    zip_file = os.path.join(os.path.expanduser("~"), "Desktop", "netsh_WLAN_Report.zip")

    # Create a zip archive and add the latest report files
    with zipfile.ZipFile(wlan_report_archive_file, "w", zipfile.ZIP_DEFLATED) as archive:
        for file_path in latest_report_files:
            archive.write(file_path, os.path.basename(file_path))

    # Delete the original report files
    for file_path in latest_report_files:
        os.remove(file_path)

def get_latest_report_files(directory):
    # Get a list of all report files in the directory
    report_files = []
    for root, dirs, files in os.walk(directory):
        for file in files:
            if file.startswith("wlan-report-latest"):
                report_files.append(os.path.join(root, file))

    # Return the latest report files (up to 3 files)
    return report_files

def get_default_gateway():
    # Get the default gateway IP address
    try:
        gateways = ni.gateways()
        default_gateway = gateways['default'][ni.AF_INET][0]
        return default_gateway
    except (KeyError, IndexError):
        return None

def print_progress_bar(duration, bar_length=30):
    start_time = time.time()
    end_time = start_time + duration

    while time.time() < end_time:
        elapsed_time = time.time() - start_time
        progress = elapsed_time / duration
        filled_length = int(bar_length * progress)
        remaining_length = bar_length - filled_length
        bar = '#' * filled_length + '-' * remaining_length
        percent = round(progress * 100, 1)
        elapsed_time = round(elapsed_time, 1)
        remaining_time = round(duration - elapsed_time, 1)

        print(f"\rProgress: [{bar}] {percent}% | Elapsed: {elapsed_time}s | Remaining: {remaining_time}s", end='', flush=True)
        time.sleep(0.1)  # Adjust the sleep duration if needed

    print()

def run_iperf_test(duration=10):
    iperf_path = 'iperf3.exe'  # Path to iperf3.exe

    # Build the iperf3 command
    gateway = get_default_gateway()
    command = [iperf_path, "-c", gateway, "-t", "10", "-p", "5201", "-R", ">>", iperf_file]

    # Execute the iperf3 command
    subprocess.run(" ".join(command), shell=True)

def log_iperf3_info(duration=10):
    print("\nExecuting iperf3 test:")
    iperf_thread = threading.Thread(target=run_iperf_test, args=(duration,))
    iperf_thread.start()

    progress_thread = threading.Thread(target=print_progress_bar, args=(duration,40))
    progress_thread.start()

    # Wait for the iperf thread to finish
    iperf_thread.join()
    progress_thread.join()

    print("iperf3 test completed.\n")

def run_ping_test(total_pings):
    # Find the default gateway IP address
    default_gateway = get_default_gateway()

    if default_gateway is None:
        print("Default gateway not found. Skipping ping test.")
        return

    # Define the ping command with output redirection to ping_file
    ping_command = f"ping {default_gateway} -n {total_pings} > {ping_file}"

    print(f"Found Smartaira Default Gateway at {default_gateway}")
    print("Running ping test to default gateway...")
    print(
        "-------------------------------------------------------------------------------\nNOTE: This will take approximately 17 minutes.\nPlease let the test run to completion.\nIf you are unable to do this due to time constraints,\npress any key to save the current results and skip the rest of the test.\n-------------------------------------------------------------------------------\n")
    print(f"Default Gateway: {default_gateway}")

    # Run the ping command
    process = subprocess.Popen(ping_command, shell=True)

    # Display a spinning loader
    loader = cycle(["-", "/", "|", "\\"])
    sys.stdout.write("Working...")
    sys.stdout.flush()

    while True:
        # Check if keyboard input is available to stop the ping test
        if msvcrt.kbhit():
            key = msvcrt.getch()
            if key:
                print("\nPing test stopped by user.\n\n")
                subprocess.call(['taskkill', '/F', '/T', '/PID',  str(process.pid)])
                break

        # Display the spinning loader
        sys.stdout.write(next(loader))
        sys.stdout.flush()
        sys.stdout.write("\b")
        time.sleep(0.1)

        if process.poll() is not None:
            break

    print("\nPing test completed.")


# Main execution
if(not is_running_with_elevated_permissions()):
    print("[ERROR]: Insufficient read permissions.")
    print("This app requires administrator privileges to obtain the wlan report your system will generate.")
    print("To do this, please right-click the application icon and choose \"Run as Administrator\".")
    input("Press enter to exit.")
    exit(1)

print("Collecting Wi-Fi Environment Data.\nPlease do not close this window.\n")

networks = scan_wifi_networks()
with open(log_file, "w", encoding="utf-8") as file:
    file.write("")
with open(ping_file, "w", encoding="utf-8") as file:
    file.write("")
with open(wlan_networks_file, "w", encoding="utf-8") as file:
    file.write("")

for network in networks:
    ssid, rssi, snr = collect_wifi_info(network)
    netsh_info = get_netsh_info()
    log_pywifi_info(ssid, rssi, snr)

log_wlan_networks_info()
create_wlan_report_zip()
run_ping_test(1000)
log_iperf3_info(10)
outputToCSV(log_file, csv_file, ["Timestamp", "SSID", "RSSI", "SNR"])
outputToCSV(wlan_networks_file, wlan_networks_csv_file, ["Timestamp", "SSID", "Band", "Channel", "Signal"])
create_zip_archive()

print("-------------------------------------------------------------------------------\nThank you for helping us gather more information!")
print("There is now a file on your desktop titled \"Smartaira WiFi Buddy Report.bin\".")
print("This is a report about your Wi-Fi.")
print("Please send that file to your Smartaira support agent as an email attachment.")
input("-------------------------------------------------------------------------------\n\nPress enter to close this window.")
exit(0)